; hello
(do
  (define fold (-> (f zero list)
    (if (nil? list) zero
        (f (car list) (fold f zero (cdr list))))))

  (define fold-left (-> (f zero list)
    (if (nil? list) zero
        (fold-left f (f (car list) zero) (cdr list)))))

  (define reverse (-> (list)
    (fold-left cons () list)))

  (define map (-> (f list)
    (reverse (fold-left
      (-> (x xs) (cons (f x) xs))
      ()
      list))))

  (define list (-> rest rest))

  (define filter (-> (good? list)
    (fold
      (-> (x xs) (if (good? x) (cons x xs) xs))
      ()
      list)))

  (define even (-> (x)
    (== (mod x 2) 0)))

  (define odd (-> (x)
    (== (mod x 2) 1)))

  (map (-> (x) (+ x 1)) (filter odd (list 1 2 3 4 5))))
