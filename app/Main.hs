
import Control.Monad (forever)

import Polysemy.Error
import Polysemy

import System.IO

import Parser
import Runtime
import Eval
import Printer

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  _ <- runEval do
    runLine "(load \"stdlib.scm\")"
    forever do
      embed $ putStr "> "
      line <- embed getLine
      runLine line
      embed $ putStrLn ""
  return ()

runLine :: HasVars m => String -> Sem m ()
runLine line = do
  case parser "stdin" line of
    Left err -> embed $ print err
    Right a -> do
        res <- eval a
        embed $ print $ toDoc res
      `catch` \(failure :: Failure) -> do
        embed $ print failure
