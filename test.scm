(do
  (define acc-gen (lambda (x)
    (lambda (method arg)
      (if (== method 'add) (set! x (+ x arg))
      (if (== method 'sub) (set! x (- x arg))
        x)))))

  (define a (acc-gen 10))

  (print "a = " (a nil 0))
  (a 'add 33)
  (print "a = " (a nil 0))
  (a 'sub 44)
  (print "a = " (a nil 0)))
