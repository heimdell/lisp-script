
module Printer where

import Data.Map qualified as Map

import Text.PrettyPrint hiding ((<>))

import Program
import Color

toStringForPrint :: Prog -> String
toStringForPrint (Lit (S s)) = s
toStringForPrint other       = show $ toDoc other

toDoc = toDoc' 0

toDoc' :: Int -> Prog -> Doc
toDoc' i = \case
  Cons "quote"  (Cons x Nil) -> "'" <> toDoc'' x
  Cons "define" (Cons n (Cons b Nil)) -> ((lPR <> (kwC "define")) <+> declC (toDoc'' n)) `indent` toDoc'' b <> rPR
  Cons "->"     (Cons a (Cons b Nil)) -> ((lPR <> (kwC "->")) <+> argC (toDoc''' a)) `indent` toDoc'' b <> rPR
  Cons f xs ->
    case toDottedList xs of
      (items, Nothing)  ->  ((lPR <> hdC (toDoc'' f)) `indent` fsep (map toDoc'' items)) <> rPR
      (items, Just end) -> (((lPR <> hdC (toDoc'' f)) `indent` fsep (map toDoc'' items)) `above` ("." `indent` toDoc'' end)) <> rPR
  Nil -> kwC "()"
  Var n -> text n
  BIF n -> bifC (text n)
  Lambda args b _ -> parens $ kwC "->"    <+> argC (toDoc''' (argsToProg args)) <+> toDoc'' b
  Macro  args b   -> parens $ kwC "macro" <+> argC (toDoc''' (argsToProg args)) <+> toDoc'' b
  Lit (S s) -> litC $ text (show s)
  Lit (I i) -> litC $ integer i
  Lit (F f) -> litC $ double f
  Lit (B True) -> litC $ "#t"
  Lit (B False) -> litC $ "#f"
  Env e -> colorHere i "{" <> fsep (map (declC . text) $ Map.keys e) <> colorHere i "}"
  where
    lPR     = colorHere i "("
    rPR     = colorHere i ")"
    toDoc'' = toDoc' (i + 1)

    toDoc''' cons = case toDottedList cons of
      ([],    Just end) -> toDoc'' end
      (items, Nothing)  ->  "(" <> fsep (map toDoc'' items) <> ")"
      (items, Just end) -> ("(" <> fsep (map toDoc'' items) `above` ("." `indent` toDoc'' end)) <> ")"

instance Show Prog where
  show = show . toDoc

instance Show Args where
  show = show . argsToProg

above, indent :: Doc -> Doc -> Doc
above  a b = hang a 0 b
indent a b = hang a 2 b

color :: Color -> Doc -> Doc
color c d = zeroWidthText begin <> d <> zeroWidthText end
  where
    begin = "\x1b[" ++ toCode c ++ "m"
    end   = "\x1b[0m"

kwC, declC, argC, litC, bifC, hdC :: Doc -> Doc
kwC   = color (faint  green)
declC = color (bright yellow)
argC  = color (faint  yellow)
litC  = color         magenta
bifC  = color (faint  red)
hdC   = color (faint  blue)

colorHere :: Int -> Doc -> Doc
colorHere i = color (list !! i')
  where
    list = [faint red, faint yellow, faint green, faint cyan, blue, faint blue, faint magenta]
    i'   = i `mod` length list
