
module Runtime where

import Polysemy
import Polysemy.State
import Polysemy.Error

import Data.Map qualified as Map
import Data.Map (Map)

import Program
import Printer ()

type Memory = Map Int Prog
type Addr   = Int

type HasVars (m :: EffectRow) = Members '[State Env, State Memory, State Addr, Error Failure, Embed IO] m

data Failure
  = IncorrectArg String Prog
  | ExpectedMoreArgs
  | CantBeArgList Prog
  | Undefined Name
  | OOMRead Addr
  | ShapeMismatch Args Prog
  | ConsCellsAreUnallowed Prog
  | CannotApply Prog Prog
  | NoOverloadExists Prog
  | ParseError Unshown
  deriving stock (Eq, Ord, Show)

newtype Unshown = Unshown { shown :: String }
  deriving stock (Eq, Ord)

instance Show Unshown where
  show = shown

runEval
  :: Sem [Error Failure, State Env, State Memory, State Addr, Embed IO] a
  -> IO (Addr, (Memory, (Env, Either Failure a)))
runEval
  = runM
  . runState 0
  . runState mempty
  . runState mempty
  . runError

progToList :: HasVars m => Prog -> Sem m [Prog]
progToList (Cons x xs) = (x :) <$> progToList xs
progToList  Nil        = return []
progToList  other      = throw $ ConsCellsAreUnallowed other

expectedArg :: Name -> Prog -> Failure -> Failure
expectedArg n p = \case
  ShapeMismatch ns ps -> ShapeMismatch (Arg n ns) (Cons p ps)
  e                   -> e

substitute :: Map Name Prog -> Prog -> Prog
substitute subst v = case v of
  Cons a b -> Cons (substitute subst a) (substitute subst b)
  Var  n   -> maybe (Var n) id $ Map.lookup n subst
  BIF  n   -> maybe (BIF n) id $ Map.lookup n subst
  Lit  lit -> Lit lit
  Nil      -> Nil

  Lambda args body env -> Lambda args (substitute (except args subst) body) env
  Macro  args body     -> Macro  args (substitute (except args subst) body)


except :: Args -> Map Name Prog -> Map Name Prog
except (Arg  n rest) subst = except rest (Map.delete n subst)
except (Rest n)      subst = Map.delete n subst
except  NoArgs       subst = subst

allocate :: HasVars m => Prog -> Sem m Int
allocate val = do
  ptr <- get @Addr
  modify $ Map.insert ptr val
  modify @Addr (+ 1)
  return ptr

newVar :: HasVars m => Name -> Prog -> Sem m ()
newVar name val = do
  addr <- allocate val
  modify @Env (Map.insert name addr)

setVar :: HasVars m => Name -> Prog -> Sem m ()
setVar name val = do
  addr <- getAddr name
  writeAddr addr val

getVar :: HasVars m => Name -> Sem m Prog
getVar name = do
  addr <- getAddr name
  readAddr addr

getAddr :: HasVars m => Name -> Sem m Int
getAddr name = do
  gets @Env (Map.lookup name) >>= \case
    Just it -> return it
    Nothing -> throw $ Undefined name

readAddr :: HasVars m => Int -> Sem m Prog
readAddr addr = do
  gets @Memory (Map.lookup addr) >>= \case
    Just v -> return v
    Nothing -> throw $ OOMRead addr

writeAddr :: HasVars m => Int -> Prog -> Sem m ()
writeAddr addr val = do
  modify @Memory $ Map.insert addr val
