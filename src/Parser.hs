
module Parser where

import Control.Applicative

import Data.Bifunctor (first)
import Data.Maybe (fromMaybe)

import Program

import Text.Parsec hiding ((<|>), many)
import Text.Parsec.String
import Text.Parsec.Token

parser :: String -> String -> Either String Prog
parser fname text = first show $ parse prog fname text

lexer :: Stream s m Char => GenTokenParser s u m
lexer = makeTokenParser LanguageDef
  { commentStart    = ""
  , commentEnd      = ""
  , commentLine     = ";"
  , nestedComments  = False
  , identStart      = oneOf $ ['a' .. 'z'] ++ ['A'.. 'Z'] ++ "~!@#$%^&*-+=?/<>\\|:"
  , identLetter     = oneOf $ ['a' .. 'z'] ++ ['A'.. 'Z'] ++ "~!@#$%^&*-+=?/<>\\|:" ++ ['0'.. '9']
  , opStart         = empty
  , opLetter        = empty
  , reservedNames   = []
  , reservedOpNames = []
  , caseSensitive   = False
  }

prog :: Parser Prog
prog = do
  whiteSpace lexer
  expr

expr :: Parser Prog
expr = choose
  [ list
  , quoted
  , stringLit
  , integerLit
  , floatLit
  , booleanLit
  , symbol
  ] <* whiteSpace lexer
  where
    list = parens lexer do
      xs <- many expr
      mend <- optionMaybe do
        _ <- dot lexer
        expr
      return $ foldr Cons (fromMaybe Nil mend) xs

    quoted = do
      char '\'' <* whiteSpace lexer
      e <- expr
      return $ Cons "quote" $ Cons e Nil

    stringLit  = (Lit . S) <$> stringLiteral lexer
    integerLit = (Lit . I) <$> decimal lexer
    floatLit   = (Lit . F) <$> float lexer

    booleanLit = choose
      [ Lit (B True)  <$ string "#t"
      , Lit (B False) <$ string "#f"
      ]

    symbol = Var <$> identifier lexer


choose :: Alternative f => [f a] -> f a
choose = foldr (<|>) empty
