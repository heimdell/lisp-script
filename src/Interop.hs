
module Interop where

import Polysemy
import Polysemy.Error

import Program
import Runtime

import Debug.Trace

newtype Symbol = Symbol Name
  deriving stock (Eq, Ord)

class AsValue v where
  fromValue   :: Prog -> Maybe v
  toValue     :: v -> Prog
  description :: String

instance AsValue Prog where
  fromValue   = Just
  toValue     = id
  description = "value"

instance AsValue () where
  fromValue   = \case
    Nil -> Just ()
    _   -> Nothing
  toValue  () = Nil
  description = "()"

instance AsValue Env where
  fromValue   = \case
    Env e -> Just e
    _     -> Nothing
  toValue     = Env
  description = "environment"

instance AsValue Symbol where
  fromValue = \case
    Var n -> Just (Symbol n)
    _     -> Nothing
  toValue (Symbol n) = Var n
  description = "symbol"

newtype Str = Str String
  deriving stock (Eq, Ord)

instance AsValue Str where
  fromValue   = \case
    Lit (S s) -> Just (Str s)
    _         -> Nothing
  toValue (Str s) = Lit (S s)
  description = "symbol"

instance AsValue Bool where
  fromValue   = \case
    Lit (B s) -> Just s
    _         -> Nothing
  toValue s = Lit (B s)
  description = "boolean"

instance AsValue Args where
  fromValue = \case
    Cons (Var n) r -> Arg n <$> fromValue r
    Var n          -> Just $ Rest n
    Nil            -> Just $ NoArgs
    _              -> Nothing

  toValue = argsToProg

  description = "args"

data Decl = Decl Symbol Prog

instance AsValue Decl where
  fromValue = \case
    Cons a (Cons b Nil) -> traceShow (a, b) $ Decl <$> fromValue a <*> fromValue b
    _                   -> Nothing

  toValue (Decl n b) = Cons (toValue n) (Cons (toValue b) Nil)

  description = "declaration"

instance AsValue a => AsValue [a] where
  fromValue = \case
    Cons a r -> (:) <$> fromValue a <*> fromValue r
    Nil      -> return []
    _        -> Nothing

  toValue = foldr Cons Nil . map toValue

  description = description @a ++ "s"

type Cons = (Prog, Prog)

instance AsValue Cons where
  fromValue = \case
    Cons a b -> Just (a, b)
    _        -> Nothing

  toValue (a, b) = Cons a b

  description = "cons"

newtype List = List Prog

instance AsValue List where
  fromValue = \case
    Cons x Nil -> Just $ List x
    _          -> Nothing

  toValue (List x) = Cons x Nil

  description = "list"

data Number
  = Float   Double
  | Integer Integer

instance Eq  Number where (==)    = binOp (==)    (==)
instance Ord Number where compare = binOp compare compare

instance AsValue Number where
  fromValue = \case
    Lit (I i) -> Just $ Integer i
    Lit (F f) -> Just $ Float   f
    _         -> Nothing

  toValue (Integer i) = Lit (I i)
  toValue (Float   f) = Lit (F f)

  description = "number"

instance Num Number where
  (+)    = binArith (+)    (+)
  (-)    = binArith (-)    (-)
  (*)    = binArith (*)    (*)
  abs    = unArith  abs    abs
  signum = unArith  signum signum
  fromInteger = Integer

binOp
  :: (Integer -> Integer -> a)
  -> (Double  -> Double  -> a)
  ->  Number  -> Number  -> a
binOp (+?) (+!) a' b' = case (a', b') of
  (Integer a, Integer b) -> a +? b
  (Integer a, Float   b) -> fromInteger a +! b
  (Float   a, Integer b) -> a +! fromInteger b
  (Float   a, Float   b) -> a +! b

binArith
  :: (Integer -> Integer -> Integer)
  -> (Double  -> Double  -> Double)
  ->  Number  -> Number  -> Number
binArith (+?) (+!) = binOp ((Integer .) . (+?)) ((Float .) . (+!))

unOp
  :: (Integer -> a)
  -> (Double  -> a)
  ->  Number  -> a
unOp i f a = case a of
  (Integer b) -> i b
  (Float   b) -> f b

unArith
  :: (Integer -> Integer)
  -> (Double  -> Double)
  ->  Number  -> Number
unArith i f = unOp (Integer . i) (Float . f)

pattern Is :: (AsValue v) => v -> Prog
pattern Is x <- (fromValue -> Just x)
  where
    Is x = toValue x

arg :: forall v m. (AsValue v, HasVars m) => (v -> Prog -> Sem m Prog) -> Prog -> Sem m Prog
arg k (uncons -> Just (v, rst)) = do
  mapError' (expectedArg "?" v) do
    case fromValue v of
      Just v' -> k v' rst
      Nothing -> throw $ IncorrectArg (description @v) v
arg _ v = throw $ ShapeMismatch (Arg "?" (Rest "?")) v

makeArgStrict :: forall v m. (AsValue v, HasVars m) => (Prog -> Sem m Prog) -> (v -> Prog -> Sem m Prog) -> Prog -> Sem m Prog
makeArgStrict force k (uncons -> Just (v, rst)) = do
  v' <- force v
  mapError' (expectedArg "?" v') do
    case fromValue v' of
      Just v'' -> k v'' rst
      Nothing  -> throw $ IncorrectArg (description @v) v
makeArgStrict _ _ v = throw $ ShapeMismatch (Arg "?" (Rest "?")) v

rest :: forall a v m. (AsValue a, AsValue v, HasVars m) => ([a] -> Sem m v) -> Prog -> Sem m Prog
rest k v = case traverse fromValue (toRestList v) of
  Just as -> fmap toValue $ k as
  Nothing -> throw $ IncorrectArg (description @[a]) v

makeRestStrict :: (HasVars m, AsValue v) => (Prog -> Sem m Prog) -> ([Prog] -> Sem m v) -> Prog -> Sem m Prog
makeRestStrict force k v = do
  fmap toValue $ k =<< traverse force (toRestList v)

mapError' :: HasVars m => (Failure -> Failure) -> Sem m a -> Sem m a
mapError' f act = act `catch` (throw . f)

noArgs :: (AsValue v, HasVars m) => Sem m v -> Prog -> Sem m Prog
noArgs res Nil = fmap toValue res
noArgs _   v   = throw $ ShapeMismatch NoArgs v

select :: forall m. (HasVars m) => [Prog -> Sem m Prog] -> Prog -> Sem m Prog
select [] v = throw $ NoOverloadExists v
select (f : fs) v = do
  f v `catch` \case
    ShapeMismatch {} -> select fs v
    IncorrectArg  {} -> select fs v
    e                -> throw e
