
module Program where

import Data.Map (Map)
import Data.String
import Data.List (unfoldr)
import Data.Bifunctor (first)

data Prog
  = Cons   Prog Prog
  | Nil
  | Var    Name
  | BIF    Name
  | Lambda Args Prog Env
  | Macro  Args Prog
  | Lit    Lit
  | Env    Env
  deriving stock (Eq, Ord)

data Lit
  = I Integer
  | S String
  | F Double
  | B Bool
  deriving stock (Eq, Ord)

type Name = String

instance IsString Prog where
  fromString = Var

type Env = Map Name Int

data Args
  = Arg Name Args
  | NoArgs
  | Rest Name
  deriving stock (Eq, Ord)

traverseProg :: Monad m => (Prog -> m Prog) -> Prog -> m Prog
traverseProg f (Cons a b) = Cons <$> f a <*> traverseProg f b
traverseProg _ Nil        = return Nil
traverseProg f a          = f a

uncons :: Prog -> Maybe (Prog, Prog)
uncons (Cons x xs) = Just (x, xs)
uncons  Nil        = Nothing
uncons  a          = Just (a, Nil)

toRestList :: Prog -> [Prog]
toRestList = unfoldr uncons

toDottedList :: Prog -> ([Prog], Maybe Prog)
toDottedList (Cons a b) = first (a :) $ toDottedList b
toDottedList  Nil       = ([], Nothing)
toDottedList  a         = ([], Just a)

argsToProg :: Args -> Prog
argsToProg = \case
  Arg  n rest -> Cons (Var n) (argsToProg rest)
  Rest n      -> Var n
  NoArgs      -> Nil

typeOf :: Prog -> String
typeOf = \case
  Cons   {} -> "list"
  Nil       -> "list"
  Var    {} -> "symbol"
  BIF    {} -> "builtin"
  Lambda {} -> "lambda"
  Macro  {} -> "macro"
  Lit  I {} -> "integer"
  Lit  F {} -> "float"
  Lit  S {} -> "string"
  Lit  B {} -> "boolean"
  Env    {} -> "environment"
