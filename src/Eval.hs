module Eval where

import Polysemy
import Polysemy.State
import Polysemy.Error

import Data.Map qualified as Map
import Data.Map (Map)
import Data.Fixed (mod')
import Data.Traversable (for)

import Program
import Runtime
import Interop
import Printer
import Parser

import Debug.Trace

eval :: forall m. HasVars m => Prog -> Sem m Prog
eval prog = do
  -- env <- get @Env
  -- env' <- traverse readAddr env
  -- embed $ putStrLn "---"
  -- embed $ print prog
  -- embed $ putStrLn $ unlines $ map show $ Map.assocs env'
  res <- case prog of
    Var n -> do
      getVar n `catch` \case
        Undefined _ | Map.member n (stdlib @m) -> return $ BIF n
        _                                      -> return $ Var n

    Cons f xs -> do
      eval f >>= (`apply` xs)

    other -> return other
  -- traceShowM ("=>", prog, res)
  return res

apply :: forall m. HasVars m => Prog -> Prog -> Sem m Prog
apply f xs = do
  -- traceShowM ("apply", f, xs)
  case f of
    BIF name -> do
      let fun = stdlib Map.! name
      -- traceShowM ("BIF", f, xs)
      fun xs

    Macro args body -> do
      subst  <- unify args xs
      subst' <- traverse readAddr subst
      eval $ substitute subst' body

    Lambda args body env -> do
      xs'   <- traverseProg eval xs
      delta <- unify args xs'
      old   <- get @Env
      put (delta <> env)
      eval body <* put old

    Cons {} -> do
      eval f >>= (`apply` xs)

    other -> throw $ CannotApply other xs

arg' :: forall v m. (AsValue v, HasVars m) => (v -> Prog -> Sem m Prog) -> Prog -> Sem m Prog
arg' = makeArgStrict eval

rest' :: forall v m. (HasVars m, AsValue v) => ([Prog] -> Sem m v) -> Prog -> Sem m Prog
rest' = makeRestStrict eval

unify :: HasVars m => Args -> Prog -> Sem m Env
unify (Arg n rst) (uncons -> Just (x, xs)) = do
  mapError' (expectedArg n x) do
    addr <- allocate x
    Map.insert n addr <$> unify rst xs

unify NoArgs Nil = mempty
unify (Rest n) xs = do
  addr <- allocate xs
  return $ Map.singleton n addr
unify args values = throw $ ShapeMismatch args values

stdlib :: forall m. HasVars m => Map Name (Function m)
stdlib = Map.fromList
  [ (,) "define" $ select
    [ arg @Symbol \(Symbol sym) ->
      arg @Prog   \val ->
      noArgs do
        newVar sym Nil
        res <- eval val
        setVar sym res

    , arg' @Str  \(Str sym) ->
      arg  @Prog \val ->
      noArgs do
        newVar sym Nil
        res <- eval val
        setVar sym res
    ]

  , (,) "get" $ arg @Symbol \(Symbol sym) -> noArgs do getVar sym
  , (,) "set!" $
      arg  @Symbol \(Symbol sym) ->
      arg' @Prog   \val ->
        noArgs do
          setVar sym val

  , (,) "typeof" $ arg' @Prog \val       -> noArgs do return (Str (typeOf val))
  , (,) "symbol" $ arg' @Str  \(Str sym) -> noArgs do return (Symbol sym)

  , (,) "if" $ arg' \cond -> arg \yes -> arg \no ->
        noArgs do if cond then eval yes else eval no

  , (,) "cons?" $
      arg' @Prog \val ->
        noArgs do
          case val of
            Cons {} -> return True
            _       -> return False

  , (,) "nil?" $
      arg' @Prog \val ->
        noArgs do
          case val of
            Nil -> return True
            _   -> return False

  , (,) "cons" $ arg' \a -> arg' \b -> noArgs do return (Cons a b)

  , (,) "car" $ arg' @Cons \(a, _) -> noArgs do return a
  , (,) "cdr" $ arg' @Cons \(_, d) -> noArgs do return d

  , (,) "quote" $ arg  @Prog \val -> noArgs do return val
  , (,) "eval"  $ select
      [ arg @Prog \val -> noArgs do eval =<< eval val
      , arg @Prog \val -> arg' @Env \env -> do
          noArgs do
            old <- get @Env
            put env
            eval =<< eval val
            put old
      ]

  , (,) "environment" $ noArgs do Env <$> get

  , (,) "builtins" $ noArgs do return $ foldr Cons Nil $ map Var $ Map.keys (stdlib @m)

  , (,) "++" $ arg' @Str \(Str a) -> arg @Str \(Str b) -> noArgs do return (Str (a ++ b))
  , (,) "/=" $ select
      [ arg' @Str \a -> arg' @Str \b -> noArgs do return (a /= b)
      , arg' @Number \a -> arg' @Number \b -> noArgs do return (a /= b)
      , arg' @Symbol \a -> arg' @Symbol \b -> noArgs do return (a /= b)
      ]

  , (,) "==" $ select
      [ arg' @Str \a -> arg' @Str \b -> noArgs do return (a == b)
      , arg' @Number \a -> arg' @Number \b -> noArgs do return (a == b)
      , arg' @Symbol \a -> arg' @Symbol \b -> noArgs do return (a == b)
      ]

  , (,) "<=" $ select
      [ arg' @Str \a -> arg' @Str \b -> noArgs do return (a <= b)
      , arg' @Number \a -> arg' @Number \b -> noArgs do return (a <= b)
      ]

  , (,) ">=" $ select
      [ arg' @Str \a -> arg' @Str \b -> noArgs do return (a >= b)
      , arg' @Number \a -> arg' @Number \b -> noArgs do return (a >= b)
      ]

  , (,) "<" $ select
      [ arg' @Str \a -> arg' @Str \b -> noArgs do return (a < b)
      , arg' @Number \a -> arg' @Number \b -> noArgs do return (a < b)
      ]

  , (,) ">" $ select
      [ arg' @Str \a -> arg' @Str \b -> noArgs do return (a > b)
      , arg' @Number \a -> arg' @Number \b -> noArgs do return (a > b)
      ]

  , (,) "mod" $ arg' @Number \a -> arg' @Number \b -> noArgs do return (binArith mod mod' a b)
  , (,) "/"   $ arg' @Number \a -> arg' @Number \b -> noArgs do return (binArith div (/) a b)
  , (,) "+"   $ arg' @Number \a -> arg' @Number \b -> noArgs do return (binArith (+) (+) a b)
  , (,) "-"   $ arg' @Number \a -> arg' @Number \b -> noArgs do return (binArith (-) (-) a b)
  , (,) "*"   $ arg' @Number \a -> arg' @Number \b -> noArgs do return (binArith (*) (*) a b)

  , (,) "read" $ select
      [ noArgs do
          embed $ putStr "? "
          line <- embed getLine
          case parser "<stdin>" line of
            Right res -> return res
            Left  err -> throw $ ParseError $ Unshown err

      , arg' @Str \(Str prompt) ->
          noArgs do
            embed $ putStr prompt
            line <- embed getLine
            case parser "<stdin>" line of
              Right res -> return res
              Left  err -> throw $ ParseError $ Unshown err
      ]

  , (,) "load" $
      arg' @Str \(Str fin) ->
        noArgs do
          line <- embed $ readFile fin
          case parser fin line of
            Right res -> do
              embed $ print $ toDoc res
              eval res
            Left  err -> throw $ ParseError $ Unshown err

  , (,) "recursive" $
      rest @Decl \decls -> do
        for decls \(Decl (Symbol name) _) -> do
          newVar name Nil

        for decls \(Decl (Symbol name) body) -> do
          body' <- eval body
          setVar name body'

        return Nil

  , (,) "do" $
      rest' \vals -> do
        -- traceShowM vals
        return $ last vals

  , (,) "->" $
      arg @Args \args ->
      arg @Prog \body ->
        noArgs do
          -- traceShowM ("lambda", args, body)
          env <- get
          return $ Lambda args body env

  , (,) "macro" $
      arg @Args \args ->
      arg @Prog \body ->
        noArgs do
          return $ Macro args body

  , (,) "print" $
      rest' \args -> do
        embed $ putStrLn $ toStringForPrint =<< args
        return ()
  ]

type Function m = Prog -> Sem m Prog
